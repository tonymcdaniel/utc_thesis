# README #

This LaTeX template has been updated to match the requirements of the Graduate School at the University of Tennessee at Chattanooga (UTC) as of April 2014.

Please check with the Graduate School for any additional formatting changes before submitting your thesis or dissertation using this template.