\NeedsTeXFormat{LaTeX2e}%
\def\filename{utthesis.cls}%
\def\fileversion{ver 1.0.0}%
\def\filedate{October 25, 2013}%
\def\contributors{Tony McDaniel}%
\typeout{\filename}%
\typeout{\fileversion}%
\typeout{Created by Tony McDaniel}%
\typeout{\filedate}%
\typeout{Some parts and settings of this class are based on the template provided by Tony Saad.}
%---------------------------------------------------------------------------------------------------%
\ProvidesClass{utthesis}
% some internal variables
\def\thesisID{1}%
\def\dissertationID{2}%
\def\theDocumentType#1{\gdef\@theDocumentType{#1}}%
\def\documentID#1{\gdef\@documentID{#1}}% documentID is either 1 (for thesis) or 2 (for dissertation)
% declare package options
\DeclareOption{thesis}{%
    \def\@theDocumentType{thesis}%
    \def\@documentID{\thesisID}}%
\DeclareOption{dissertation}{%
    \def\@theDocumentType{dissertation}%
    \def\@documentID{\dissertationID}}%
\DeclareOption{linespacing}{\@lnspacing}
\DeclareOption*{\PassOptionsToPackage{\CurrentOption}{color}}%
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{report}}%
\ExecuteOptions{dissertation,letterpaper,12pt}% execute default options
\ProcessOptions%
\LoadClass{report} % single-sided
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 	Main Definitions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\def\copyrightYear#1{\gdef\@copyrightYear{#1}}%
\def\graduationMonth#1{\gdef\@graduationMonth{#1}}%
\def\majorProfessor#1{\gdef\@majorProfessor{#1}}%
\def\keywords#1{\gdef\@keywords{#1}}%
\def\major#1{\gdef\@major{#1}}%
\def\degree#1{\gdef\@degree{#1}}%
\def\college#1{\gdef\@college{#1}}%
\def\dept#1{\gdef\@dept{#1}}%
\def\university#1{\gdef\@university{#1}}%
\def\numberOfCommitteeMembers#1{\gdef\@numberOfCommitteeMembers{#1}} % enter the number of committee members
\def\committeeMemberA#1 {\gdef\@committeeMemberA{#1}}% first committee member
\def\committeeMemberB#1 {\gdef\@committeeMemberB{#1}}% second committee member
\def\committeeMemberC#1 {\gdef\@committeeMemberC{#1}}% ... you get the trend!
\def\committeeMemberD#1 {\gdef\@committeeMemberD{#1}}%
\def\committeeMemberE#1 {\gdef\@committeeMemberE{#1}}%
\def\committeeMemberTitleA #1 {\gdef\@committeeMemberTitleA{#1}}
\def\committeeMemberTitleB #1 {\gdef\@committeeMemberTitleB{#1}}
\def\committeeMemberTitleC #1 {\gdef\@committeeMemberTitleC{#1}}
\def\committeeMemberTitleD #1 {\gdef\@committeeMemberTitleD{#1}}
\def\committeeMemberTitleE #1 {\gdef\@committeeMemberTitleE{#1}}
\def\committeeMemberRoleA #1 {\gdef\@committeeMemberRoleA{#1}}
\def\committeeMemberRoleB #1 {\gdef\@committeeMemberRoleB{#1}}
\def\committeeMemberRoleC #1 {\gdef\@committeeMemberRoleC{#1}}
\def\committeeMemberRoleD #1 {\gdef\@committeeMemberRoleD{#1}}
\def\committeeMemberRoleE #1 {\gdef\@committeeMemberRoleE{#1}}
% initialize macros
\def\@title{My Title}
\def\@author{My Name}
\def\@keywords{My Keywords}
\def\@graduationMonth{May}
\def\@copyrightYear{20013}
\def\@degree{Master of Science}	% degree type
\def\@college{Engineering and Computer Science}           % college
\def\@dept{Mechanical, Aerospace and Biomedical Engineering}	% department
\def\@major{Mechanical Engineering}	% major
\def\@university{The University  of Tennessee at Chattanooga}	% school
\def\@numberOfCommitteeMembers{3} % enter the number of committee members
\def\@committeeMemberA {First Member}	% name of first committee member
\def\@committeeMemberTitleA {First Title}
\def\@committeeMemberRoleA {(First Role)}
\def\@committeeMemberB {Second Member}	% name of second committee member
\def\@committeeMemberTitleB {Second Title}
\def\@committeeMemberRoleB {(Second Role)}
\def\@committeeMemberC {Third Member}	% name of second committee member
\def\@committeeMemberTitleC {Third Title}
\def\@committeeMemberRoleC {(Third Role)}
\def\@committeeMemberD {Fourth Member}	% name of second committee member
\def\@committeeMemberTitleD {Fourth Title}
\def\@committeeMemberRoleD {(Fourth Role)}
\def\@committeeMemberE {Fifth Member}	% name of second committee member
\def\@committeeMemberTitleE {Fifth Title}
\def\@committeeMemberRoleE {(Fifth Role)}
\def\@committeeMemberF {Fifth Member}	% name of second committee member
\def\@committeeMemberTitleF {Fifth Title}
\def\@committeeMemberRoleF {(Fifth Role)}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 	LOAD PACKAGES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\RequirePackage{amsmath,amsthm,amssymb}     % AMS math, AMS theorem, AMS fonts
\RequirePackage{setspace}                   % line spacing
\RequirePackage[vcentering,dvips]{geometry}	% help with margins
\RequirePackage[pdftex,plainpages=false,pdfpagelabels=true,breaklinks=true]{hyperref} % for pdf bookmarks
\RequirePackage{color}
\RequirePackage{multicol}
% define theorem, corollary, and lemma environments
\theoremstyle{plain}
\newtheorem*{theorem*}{Theorem}
\newtheorem{theorem}{Theorem}[chapter]
\newtheorem{corollary}{Corollary}[theorem]
\newtheorem{lemma}{Lemma}[theorem]
\let\origdoublepage\cleardoublepage
\newcommand{\clearemptydoublepage}{%
  \clearpage
  {\pagestyle{empty}\origdoublepage}%
}
\let\cleardoublepage\clearemptydoublepage
\hypersetup{%
	bookmarksnumbered = true,
	pdftitle={\@title},
	pdfauthor={\@author},
	pdfsubject={\@dept},
	pdfkeywords={\@keywords},
	pdfpagelayout=SinglePage,
    bookmarksopen=False,
	pdfborder=0 0 0, 		% make all links invisible, so the pdf looks good when printed	
    pdffitwindow=true,      % window fit to page when opened
    pdfcreator={\@author},  % creator of the document
    pdfnewwindow=true,      % links in new window
    colorlinks=true,        % false: boxed links; true: colored links
    linkcolor=blue,         % color of internal links
    citecolor=magenta,      % color of links to bibliography
    filecolor=magenta,      % color of file links
    urlcolor=cyan           % color of external links
}
\geometry{
    letterpaper,
    left = 1.0in,
    right = 1.0in,
    top = 1.0in,
    bottom = 1.2in
}
\hyphenpenalty=5000	% reduce hyphenation as much as possible
\tolerance=1000     % goes with the previous command
% Widow/orphan protection
\clubpenalty=10000    % Minimize orphans orphans(eliminate 10000)
\widowpenalty=10000   % Minimize widows
\brokenpenalty=10000  % Do not allow hyphen across pages
% Use cool footnotes
\renewcommand{\thefootnote}{\fnsymbol{footnote}}
\raggedbottom
\pagestyle{plain}

\renewcommand{\@makechapterhead}[1]{%
  \vspace*{0.45in}%
	\begin{center}
	CHAPTER \thechapter 
	\mbox{} \\
	\uppercase{#1}
	\mbox{} \\
	\end{center}
}

\renewcommand{\@makeschapterhead}[1]{%
  \vspace*{0.45in}%
	\begin{center}
	\uppercase{#1}
	\mbox{} \\
	\end{center}
}

\renewcommand\section{\@startsection {section}{1}{\z@}%
	{-3.5ex \@plus -1ex \@minus -.2ex}%
	{2.3ex \@plus.2ex}%
	{\normalsize}}

\renewcommand\subsection{\@startsection {section}{1}{\z@}%
	{-3.5ex \@plus -1ex \@minus -.2ex}%
	{2.3ex \@plus.2ex}%
	{\normalsize\centering}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% makeTitlePage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand{\makeTitlePage} {
  %\thispagestyle{empty}
    \vspace*{0.5in}
    \begin{centering}
        \begin{spacing}{2.0}
            \@title\\
            \vspace{5pc}
            By\\
            %\vspace{12pt}
            \@author\\
        \end{spacing}
        \vspace{12pt}
    \end{centering}
    \vspace*{5pc}
  \begin{spacing}{1.0}
    \begin{center}
        \rm
        \ifnum\@documentID=\thesisID {
          A Thesis Submitted to the Faculty of the University\\
          of Tennessee at Chattanooga in Partial\\
          Fulfillment of the Requirements of the\\
          Degree of \@degree \\
        } \else {
            A Dissertation Submitted to the Faculty of the University\\
          of Tennessee at Chattanooga in Partial\\
          Fulfillment of the Requirements of the\\
          Degree of \@degree \\
        } \fi
      \vspace*{24pt}
      \@university \\
      Chattanooga, Tennessee \\
      \vspace*{12pt}
      \@graduationMonth\ \@copyrightYear
      \vspace*{1.0in}
    \end{center}
  \end{spacing}
  \cleardoublepage
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% makeApprovalPage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand {\makeApprovalPage} {
    \thispagestyle{empty}
    \vspace*{0.5in}
    \begin{centering}
        \begin{spacing}{2.0}
            \@title\\
            \vspace{5pc}
            By\\
            %\vspace{12pt}
            \@author\\
        \end{spacing}
        \vspace{12pt}
    \end{centering}
    \begin{spacing}{1.0}
        \vspace*{2pc}
        Approved:\\
        \begin{tabular}[H]{lll}
            \hspace*{0.5in}\\[12pt]
            \underline{\hspace*{3in}}    &\hspace*{0.15in}   & \underline{\hspace*{3in}}\\[2pt]
            \@committeeMemberA           &\hspace*{0.15in}   & \@committeeMemberB       \\
            \@committeeMemberTitleA      &\hspace*{0.15in}   & \@committeeMemberTitleB  \\
            (\@committeeMemberRoleA)     &\hspace*{0.15in}   & (\@committeeMemberRoleB) \\
            \vspace*{2pc}\\
            \underline{\hspace*{3in}}    &\hspace*{0.15in}   & \underline{\hspace*{3in}}\\[2pt]
            \@committeeMemberC           &\hspace*{0.15in}   & \@committeeMemberD       \\
            \@committeeMemberTitleC      &\hspace*{0.15in}   & \@committeeMemberTitleD  \\
            (\@committeeMemberRoleC)     &\hspace*{0.15in}   & (\@committeeMemberRoleD) \\
            \vspace*{2pc}\\
            \underline{\hspace*{3in}}    &\hspace*{0.15in} & \\[2pt]
            \@committeeMemberE           &\hspace*{0.15in} & \\
            \@committeeMemberTitleE      &\hspace*{0.15in} & \\
            \@committeeMemberRoleE     &\hspace*{0.15in}   & \\
        \end{tabular}
    \end{spacing}
    \pagebreak%
}
\newcommand{\makeAppendixPage}{
    \newpage
    \vspace*{3.5in}
    \begin{center}\normalsize \doublespacing  \rm{APPENDIX \\EIGENSYSTEM} \end{center}
    \newpage
}
\newcommand{\makeAbstractPage}{
    \newpage
    % \addToPDFBookmarks{1}{Abstract}{e} % add a pdf bookmark to the abstract page
    \vspace*{0.5in}
    \begin{centering}
        \begin{spacing}{2.0}
            \normalsize \rm{ABSTRACT}
        \end{spacing}
        \vspace{12pt}
    \end{centering}
    \begin{spacing}{1.0}
        \input{front-matter/abstract} % your abstract
    \end{spacing}
    \newpage
}
\newcommand{\makeDedicationPage}{
    % \addToPDFBookmarks{1}{Dedication}{b} % add a pdf bookmark to the dedication page
    \addToTOC{DEDICATION}
    \newpage
    \vspace*{0.5in}
    \begin{centering}
        \begin{spacing}{2.0}
            \normalsize \rm{DEDICATION}
        \end{spacing}
        \vspace{12pt}
    \end{centering}
    \begin{spacing}{1.0}
        \input{front-matter/dedication} % include the dedication
    \end{spacing}
    \newpage
}
\newcommand{\makeAcknowledgementsPage}{
    % \addToPDFBookmarks{1}{Acknowledgements}{c} % add a pdf bookmark to the acknowledgements page
    \addToTOC{ACKNOWLEDGEMENTS}
    \newpage
    \vspace*{0.5in}
    \begin{centering}
        \begin{spacing}{2.0}
            \normalsize \rm{ACKNOWLEDGEMENTS}
        \end{spacing}
        \vspace{12pt}
    \end{centering}
    \begin{spacing}{1.0}
        \input{front-matter/acknowledgements} % include the acknowledgements
    \end{spacing}
    \newpage
}
\newcommand{\makeVitaPage}{
    % \addToPDFBookmarks{1}{Vita}{b} % add a pdf bookmark to the dedication page
    \addToTOC{VITA}
    \newpage
    \vspace*{0.5in}
    \begin{centering}
        \begin{spacing}{2.0}
            \normalsize \rm{VITA}
        \end{spacing}
        \vspace{12pt}
    \end{centering}
    \begin{spacing}{1.0}
        \input{back-matter/vita}
    \end{spacing}
    \newpage
}
\newcommand {\addToTOC}[1] {
	\cleardoublepage
	\phantomsection
	\addcontentsline{toc}{part}{#1}
}
\newcommand {\addToPDFBookmarks}[3] {
	\cleardoublepage
	\phantomsection
	\pdfbookmark[#1]{#2}{#3}
}
\newcommand {\makeCopyrightPage} {
    \phantom{.}
    \vspace*{3in}
    \begin{center}
        Copyright \copyright\  \@copyrightYear  \\
        By \@author \\
        All Rights Reserved.
    \end{center}
}
\endinput 
